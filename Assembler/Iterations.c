#include "Iterations.h"


// NEED THE FILE PATH
// CHange the tableindex to be ptr

void firstIteration(char* path, symbolTable table, word* cs, int* ic, word* ds, int* dc)
{
	FILE* f = fopen(path, "r");
	char* line;
	int firstCharIndex;
	int lineCounter = 1;
	bool hasLabel = FALSE;

	int tableIndex = 0;

	while (line = getline(f))
	{
		// MAYBE REPLACE WITH MACRO
		// Ignore whitespaces in start of the line
		for (firstCharIndex = 0; line[firstCharIndex] && line[firstCharIndex] == ' ' || line[firstCharIndex] == '\t'; firstCharIndex++);
		
		/* Ignore comments or empty lines */
		if (line[firstCharIndex] == ';' || firstCharIndex == strlen(line))
		{
			free(line);
			continue;
		}

		/* There is a label in the line */
		if (strcspn(line, ":") != 0 && strcspn(line, ":") != strlen(line) && isLabel(line, table, tableIndex, lineCounter))
		{
			// Skip the label and ignore whitespaces in start of the line
			firstCharIndex += strlen(table[tableIndex].label) + 1;
			for (; line[firstCharIndex] && line[firstCharIndex] == ' ' || line[firstCharIndex] == '\t'; firstCharIndex++);
			hasLabel = TRUE;
		}
	
		/* Handle all the instruction statments */
		if (isLegalData(line + firstCharIndex, table, tableIndex, ds, dc))
		{
			continue;
		}
		else if (isLegalString(line + firstCharIndex, table, tableIndex, ds, dc))
		{
			continue;
		}
		else if (isEntry(line + firstCharIndex))
		{
			strcpy(table[tableIndex].label, NULL);
			continue;
		}
		else if (isExtern(line + firstCharIndex))
		{
			hasLabel = TRUE;
			continue;
		}
		/* Handle the case where the line is operation */
		else if (isLegalOperation(line + firstCharIndex, table, tableIndex, cs, ic))
		{
			continue;
		}
		else if (table[tableIndex].label != NULL)
		{
			strcpy(table[tableIndex].label, NULL);
			// Maybe give warning after empty label
			fprintf(stderr, "Error in line %d - Syntax error after '%s'", lineCounter, table[tableIndex].label);
			errorDetected = TRUE;
			continue;
		}
		else
		{
			fprintf(stderr, "Error in line %d - Syntax error", lineCounter, table[tableIndex].label);
			errorDetected = TRUE;
		}

		// Increase the table index if there is a label in this line 
		if (hasLabel)
		{
			tableIndex++;
		}

		free(line);
	}
	fclose(f);
}

/*
	In this iteration we complete the assembling process
*/
void secondIteration(char* path, symbolTableRow* table, word* cs, int* ic, word* ds, int* dc)
{
	int i;
	FILE* ob = fopen(strcat(path, ".ob"), "w");;
	FILE* ent = fopen(strcat(path, ".ent"), "w");
	FILE* ext = fopen(strcat(path, ".ext"), "w");




	fclose(ob);
	fclose(ent);
	fclose(ext);
}

