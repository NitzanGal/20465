#include <stdio.h>
#include "Structures.h"
#include "Handler.h"
/*
	This function is reasponsible for the first part of the assembling proccess.
	What included in this function:
		- Preparing the symbol table that will be used in the second part
		- Hanle all the instruction statments (.data, .string, .entry, .extern)
		- 
	In the end of this function the symbol table is ready to be used by the second part,
	the data segment is full with all the data, the length of the code is known.
*/
void firstIteration(char*, symbolTableRow*, word*, int*, word*, int*);

/*

*/
void secondIteration(char*, symbolTableRow*, word*, int*, word*, int*);