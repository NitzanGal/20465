#include "Handler.h"

/* Handle various cases that may happen */

#define MAX_ARG_SIZE MAX_LABEL_SIZE

/*
	Find if in the given line there is label.
	If there is one so add it to the symbol table (only if it's legal, otherwise print error).
*/
bool handleLabel(char* line, symbolTable table /* maybe need to change table to symbolTable* */, int tableIndex, int lineCounter)
{
	char* label = strtok(line, ":");
	int i;

	// There is no label in this line
	if (strlen(label) == strlen(line))
	{
		return FALSE;
	}

	// Label is too long
	if (strlen(label) > MAX_LABEL_SIZE)
	{
		fprintf("Error in line %d - label is too long (max label size is 30)\n", lineCounter);
		return FALSE;
	}

	// label doesn't start with an english letter
	if (!(isalpha(label[0])))
	{
		fprintf("Error in line %d - label must start with an english letter\n", lineCounter);
		return FALSE;
	}

	// letter doesn't consist from english letters and digits
	for (i = 1; i < strlen(label); i++)
	{
		if (!(isalpha(label[i]) || isdigit(label[i])))
		{
			fprintf("Error in line %d - label must be consist from english letters and digits\n", lineCounter);
			return FALSE;
		}
	}

	for (i = 0; i < tableIndex; i++)
	{
		// Label already exist
		if (!strcmp(table[i].label, label))
		{
			fprintf("Error in line %d - label %s already exist\n", lineCounter, label);
			return FALSE;
		}
	}

	for (i = 0; registers[i]; i++){
		// Label is name of register
		if (!strcmp(label, registers[i]))
		{
			fprintf("Error in line %d - label can't be name of register", lineCounter);
			return FALSE;
		}
	}

	for (i = 0; commands[i].name; i++){
		// Label is name of operation
		if (!strcmp(label, commands[i].name))
		{
			fprintf("Error in line %d - label can't be name of operation", lineCounter);
			return FALSE;
		}
	}

	strncpy(table[tableIndex].label, label, MAX_LABEL_SIZE);
	return TRUE;
}

/*
	Find if in the given line there is .data instruction.
	If there is one so add it to the symbol table and add the values to the data segment (only if it's legal, otherwise print error).
*/
bool handleData(char* line, symbolTable table, int tableIndex, int lineCounter, word* ds, int* dc)
{
	int firstValueIndex;
	char* currValue;
	int i, oldDc = *dc;
	short value;

	if (!(strlen(line) >= strlen(".data") && line[0] == '.' && line[1] == 'd' && line[2] == 'a' && line[3] == 't' && line[4] == 'a'))
	{
		return FALSE;
	}

	for (firstValueIndex = strlen(".data"); line[firstValueIndex] && line[firstValueIndex] == ' ' || line[firstValueIndex] == '\t'; firstValueIndex++);

	currValue = strtok(line + firstValueIndex, ",");

	while (currValue)
	{
		/* There was a problem with the value */
		if (valueIsNumber(currValue, lineCounter)) 
		{
			*dc = oldDc;
			strcpy(table[tableIndex--].label, NULL);
			break;
		}
		else
		{
			fprintf(stderr, "Error in line %d - Value %s is invalid value for data instruction\n", lineCounter, currValue);
		}

		/* Convert the value from char* to short*/
		value = (short) atoi(currValue);

		/* Value is too big for being represented by 15 bits */
		if (value < -pow(2, 14) || value > -pow(2, 14) - 1)
		{
			*dc = oldDc;
			strcpy(table[tableIndex--].label, NULL);

			fprintf(stderr, "Error in line %d - Value %s is invalid value for data instruction\n", lineCounter, currValue);
			break;
		}

		/* If the .data instruction has label */
		if (table[tableIndex].label != NULL)
		{
			/* Update the symbol table */
			table[tableIndex].address = *dc;
			table[tableIndex].isExtern = FALSE;
			table[tableIndex].isOp = FALSE;
		}

		/* Update the data segment and the data counter */
		ds[(*dc)++] = value;

		/* Get the next value */
		currValue = strtok(NULL, ","); 
	}

	tableIndex++;

	return TRUE;
}

/*
	Find if in the given line there is .string instruction.
	If there is one so add it to the symbol table and add the values to the data segment (only if it's legal, otherwise print error).
*/
bool handleString(char* line, symbolTable table, int tableIndex, int lineCounter, word* ds, int* dc)
{
	int firstValueIndex, lastValueIndex, i;

	if (!(strlen(line) >= strlen(".string") && line[0] == '.' && line[1] == 's' && line[2] == 't' && line[3] == 'r' && line[4] == 'i' && line[5] == 'n' && line[6] == 'g'))
	{
		return FALSE;
	}

	// Find the bounderies of the string (")
	for (firstValueIndex = strlen(".string"); line[firstValueIndex] && line[firstValueIndex] == ' ' || line[firstValueIndex] == '\t'; firstValueIndex++);
	for (lastValueIndex = strlen(line); lastValueIndex > firstValueIndex && line[lastValueIndex] == ' ' || line[lastValueIndex] == '\t'; lastValueIndex++);

	if (line[firstValueIndex] != '\"' || line[lastValueIndex] != '\"')
	{
		strcpy(table[tableIndex--].label, NULL);
		fprintf(stderr, "Error in line %d - %s is invalid string \n", lineCounter, line + firstValueIndex);
		return FALSE;
	}

	/* If the .string instruction has label */
	if (table[tableIndex].label != NULL)
	{
		/* Update the symbol table */
		table[tableIndex].address = *dc;
		table[tableIndex].isExtern = FALSE;
		table[tableIndex++].isOp = FALSE;
	}

	/* Update the data segment and the data counter */
	for (i = firstValueIndex + 1; i < lastValueIndex; i++)
	{
		ds[(*dc)++] = (short) line[i];
	}
	ds[(*dc)++] = 0;

	return TRUE;
}

/*
	Find if in the given line there is a .string instruction.
	If there is one so add it to the symbol table and add the values to the data segment (only if it's legal, otherwise print error).
*/
bool handleEntry(char* line)
{
	return strlen(line) >= strlen(".entry") && line[0] == '.' && line[1] == 'e' && line[2] == 'n' && line[3] == 't' && line[4] == 'r' && line[5] == 'y';
}

/*
	Find if in the given line there is a .string instruction.
	If there is one so add it to the symbol table and add the values to the data segment (only if it's legal, otherwise print error).
*/
bool handleExtern(char* line, symbolTable table, int tableIndex, int lineCounter)
{
	int i, firstValueIndex;
	char* label;

	if (!(strlen(line) >= strlen(".extern") && line[1] == 'e' && line[2] == 'x' && line[3] == 't' && line[4] == 'e' && line[5] == 'r' && line[6] == 'n'))
	{
		return FALSE;
	}

	for (firstValueIndex = strlen(".string"); line[firstValueIndex] && line[firstValueIndex] == ' ' || line[firstValueIndex] == '\t'; firstValueIndex++);
	
	if (isLabel(line + firstValueIndex, table, tableIndex, lineCounter))
	{
		table[tableIndex].address = 0;
		table[tableIndex].isExtern = TRUE;
		table[tableIndex++].isOp = FALSE;
	}
	
}

/*
	Find if in the given line there is an operation.
	If there is one so add it to the symbol table and add the values to the data segment (only if it's legal, otherwise print error).
*/
bool handleCommand(char* line, symbolTable table, int tableIndex, int lineCounter, word* cs, int* ic)
{	
	char* cmd = strtok(line, " \t"); /* Check with spaces before the cmd */
	char* srcTemp = strtok(NULL, ",");
	char* dstTemp = strtok(NULL, ",");

	/* may create function to remove spaces */

	char src[MAX_ARG_SIZE] = { 0 };
	int firstCharIndex, lastCharIndex;
	for (firstCharIndex = 0; srcTemp[firstCharIndex] && srcTemp[firstCharIndex] == ' ' || srcTemp[firstCharIndex] == '\t'; firstCharIndex++);
	for (lastCharIndex = strlen(srcTemp); lastCharIndex >= 0 && srcTemp[lastCharIndex] == ' ' || srcTemp[lastCharIndex] == '\t'; lastCharIndex--);
	strncpy(src, srcTemp + firstCharIndex, lastCharIndex - firstCharIndex);

	char dst[MAX_ARG_SIZE] = { 0 };
	int firstCharIndex, lastCharIndex;
	for (firstCharIndex = 0; dstTemp[firstCharIndex] && dstTemp[firstCharIndex] == ' ' || dstTemp[firstCharIndex] == '\t'; firstCharIndex++);
	for (lastCharIndex = strlen(dstTemp); lastCharIndex >= 0 && dstTemp[lastCharIndex] == ' ' || dstTemp[lastCharIndex] == '\t'; lastCharIndex--);
	strncpy(dst, dstTemp + firstCharIndex, lastCharIndex - firstCharIndex);

	word cmdWord = 0;
	word firstWord = 0;
	word secondWord = 0;

	int cmdCode = isCmd(cmd);
	int dstOperandCode;
	int srcOperandCode;
	if (cmdCode == -1)
	{
		fprintf(stderr, "Error in line %d - No such command '%s' \n", lineCounter, cmd);
		return FALSE;
	}   

	/* Bits 2-3 represent the addressing method of the destination operand */
	if (dst)
	{
		dstOperandCode = handleArg(dst, cmdCode, TRUE, lineCounter);
		if (dstOperandCode != -1)
			cmdWord |= dstOperandCode << 2;
		else
			return FALSE;
	}
	/* Bits 4-5 represent the addressing method of the source operand */
	if (src)
	{
		srcOperandCode = handleArg(src, cmdCode, FALSE, lineCounter);
		if (srcOperandCode != -1)
			cmdWord |= srcOperandCode << 4;
		else
			return FALSE;
	}
	/* Bits 6-9 represent the command code */
	cmdWord |= cmdCode << 6;
	/* Bits 12-14 are always 1 */
	cmdWord |= 0x7000;
	/* Bits 10-11 represent the group */
	if (src && dst)
		cmdWord |= 0xC00;
	else if (src)
		cmdWord |= 0x400;
	/* Bits 0-1 represent the coding type (Absolute,Relocatable,External) */
	/*------------------if ((cmdWord & 0xC == 0x4) || (cmdWord & 0x30 == 0x10))
		cmdWord |= 0x2;-------------------*/


	cs[(*ic)++] = cmdWord; 

	switch (srcOperandCode)
	{
		case IMM:
			firstWord |= atoi(src + 1) << 2;
			cs[(*ic)++] = firstWord;
			break;
		case DIR:
			firstWord |= 0x2;
			cs[(*ic)++] = firstWord;
			break;
		case IND:
			firstWord |= atoi(src[1]) << 2;
			firstWord |= atoi(src[4]) << 8;
			cs[(*ic)++] = firstWord;
			break;
		case REG:
			firstWord |= atoi(src[1]) << 8;
			cs[(*ic)++] = firstWord;
	}

	switch (dstOperandCode)
	{
		case IMM:
			secondWord |= atoi(src + 1) << 2;
			cs[(*ic)++] = secondWord;
			break;
		case DIR:
			secondWord |= 0x2;
			cs[(*ic)++] = secondWord;
			break;
		case IND:
			secondWord |= atoi(src[1]) << 2;
			secondWord |= atoi(src[4]) << 8;
			cs[(*ic)++] = secondWord;
			break;
		case REG:
			if (srcOperandCode == REG)
			{
				firstWord |= atoi(src[1]) << 2;
				cs[(*ic) - 1] = firstWord;
			}
			else
			{
				secondWord |= atoi(src[1]) << 8;
				cs[(*ic)++] = secondWord;
			}
	}
}

/* Helper functions*/

int handleArg(char* arg, int cmdIndex, bool dstArg, int lineCounter)
{
	char* firstReg;
	char* secondReg;
	int firstCharIndex, lastCharIndex;
	int addressing;

	/* Immediate addressing */
	if (arg[0] == '#' && isNumber(arg + 1))
	{
		addressing = SRC_IMM;
	}
	/* Register addressing */
	else if (isRegister(arg) != -1)
	{
		addressing = SRC_REG;
	}
	else
	{
		/* Index addressing */
		if (arg[2] == ']' && arg[5] == ']')
		{
			firstReg = strtok(arg, "[");
			secondReg = strtok(arg, "]");

			if (isRegister(firstReg) != -1 && isRegister(firstReg) % 2 == 1 && isRegister(secondReg) != -1 && isRegister(secondReg) % 2 == 0)
			{
				addressing = SRC_IND;
			}
		}

		/* Assumeing label (in the 2nd iteration verifying that) */
		addressing = SRC_DIR;
	}

	addressing += 1;

	/* --- document this --- */
	if (dstArg)
		addressing <<= 4;

	if (commands[cmdIndex].addressingTypes & addressing)
	{
		if (dstArg)
			addressing >>= 4;
		return addressing == DST_REG ? 3 : addressing / 2;
	}
	else
	{
		fprintf(stderr, "Error in line %d - %s doesn't fit to command %s can't\n", lineCounter, arg, commands[cmdIndex].name);
		return -1;
	}
}

/*
Return the next line from the given file
*/
char* getline(FILE* f)
{
	char* line = (char*)malloc(MAX_LINE_SIZE);
	fgets(line, MAX_LINE_SIZE, f);

	// Delete \n from the end of the line
	line[strlen(line) - 1] = NULL;

	return line;
}

/*
Check out if the given char* is actually a number.
*/
bool isNumber(char* value)
{
	int i;
	int minusCounter = 0;
	for (i = 0; value[i]; i++)
	{
		/* The only char permited are numbers and spaces and '-' if after it there is a digit */
		if (!(value[i] == ' ' || value[i] == '\t' || isdigit(value[i]) || (value[i] == '-' && isdigit(value[i + 1])) || minusCounter <= 1)) // --- TO DO: Only one time - is allowed ---
		{
			return FALSE;
		}
		else if (value[i] == '-')
			minusCounter++;
	}
	return TRUE;
}

int isCmd(char* cmd)
{
	int i;
	for (i = 0; commands[i].name || strcmp(commands[i].name, cmd); i++)
	{
		return i;
	}

	return -1;
}

int isRegister(char* cmd)
{
	int i;
	for (i = 0; registers[i] || strcmp(registers[i], cmd); i++)
	{
		return i;
	}

	return -1;
}