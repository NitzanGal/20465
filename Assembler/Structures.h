/* Can be 0/1 */
typedef enum{FALSE = 0, TRUE = 1} bool;

/* A memory word of the imaginary computer */
#define SEGMENT_SIZE 1000
typedef unsigned short word;

// MAYBE DO: typedef word segment[SEGMENT_SIZE]

/* Store all the names of the operations and the registers */
#define NUM_OF_COMMANDS 16
#define NUM_OF_REGISTERS 8

typedef struct
{
	char* name;
	char addressingTypes;
} command;

/* Addressing types */
#define IMM 0
#define DIR 1
#define IND 2
#define REG 3

#define SRC_IMM 0x1
#define SRC_DIR 0x2
#define SRC_IND 0x4
#define SRC_REG 0x8

#define DST_IMM 0x10
#define DST_DIR 0x20
#define DST_IND 0x40
#define DST_REG 0x80

command commands[NUM_OF_COMMANDS + 1] = {
	{ "mov", SRC_IMM | SRC_DIR | SRC_IND | SRC_REG | DST_DIR | DST_IND | DST_REG},
	{ "cmp", SRC_IMM | SRC_DIR | SRC_IND | SRC_REG | DST_IMM | DST_DIR | DST_IND | DST_REG },
	{ "add", SRC_IMM | SRC_DIR | SRC_IND | SRC_REG | DST_DIR | DST_IND | DST_REG },
	{ "sub", SRC_IMM | SRC_DIR | SRC_IND | SRC_REG | DST_DIR | DST_IND | DST_REG},
	{ "not", DST_DIR | DST_IND | DST_REG },
	{ "clr", DST_DIR | DST_IND | DST_REG },
	{ "lea", SRC_DIR | SRC_IND | DST_DIR | DST_IND | DST_REG },
	{ "inc", DST_DIR | DST_IND | DST_REG },
	{ "dec", DST_DIR | DST_IND | DST_REG },
	{ "jmp", DST_DIR | DST_IND | DST_REG },
	{ "bne", DST_DIR | DST_IND | DST_REG },
	{ "red", DST_DIR | DST_IND | DST_REG },
	{ "prn", DST_DIR | DST_IND | DST_REG },
	{ "jsr", DST_IMM | DST_DIR | DST_IND | DST_REG },
	{ "rts", 0 },
	{ "stop", 0 },
	{ NULL, 0 }
};

char* registers[NUM_OF_REGISTERS + 1] = {
	"r0", "r1", "r2", "r3",
	"r4", "r5", "r6", "r7", NULL
};

/* Structs to store the symbol table */
#define MAX_LABEL_SIZE 30
#define SYMBOL_TABLE_SIZE 256
typedef struct {
	char label[MAX_LABEL_SIZE];
	int address;
	bool isExtern;
	bool isOp;
} symbolTableRow;
typedef symbolTableRow* symbolTable;