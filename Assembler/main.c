#include "Structures.h"
#include <stdio.h>
#include "Handler.h"



int main(int argc, char** argv)
{

	/* CHANGE ALL // TO /* --- */
	



	int i;
	/* The program must recieve at least 1 file to assemble */
	if (argc < 2)
	{
		fprintf(stderr, "No files paths were supplied.\n");
		exit(1);
	}

	// Assemble all the recieved files
	for (i = 1; i < argc; i++)
	{
		assemble(argv[i]);
	}

	return 0;
}

/*
	This function is calling the functions that does both of the iterations.
*/
void assemble(char* path)
{
	FILE* f = fopen(path, "r");
	if (f == NULL)
	{
		fprintf(stderr, "Can't open the file %s", path);
		return;
	}
	fclose(f); //?

	/*
	
		Assuming the size of the symbol table
	
	*/
	symbolTableRow symbolTable[SYMBOL_TABLE_SIZE];
	word codeSegment[SEGMENT_SIZE];
	word dataSegment[SEGMENT_SIZE];
	int ic = 100, dc = 0;

	// ---  entrys data structure ---
	// ---  externs data structure ---

	firstIteration(strcat(path, ".as"), &symbolTable, &codeSegment, ic, &dataSegment, dc);
	secondIteration(strcat(path, ".as"), &symbolTable, &codeSegment); // pass also the entrys and externs data structures

	if (errorDetected)
	{
		system(strcat("rm -f ", strcat(path, ".ob")));
		system(strcat("rm -f ", strcat(path, ".ent")));
		system(strcat("rm -f ", strcat(path, ".ext")));
	}
}
