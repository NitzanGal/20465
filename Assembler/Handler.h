#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "Structures.h"

#define MAX_LINE_SIZE 80


bool errorDetected = FALSE;

bool handleLabel(char* line, symbolTable table /* maybe need to change table to symbolTable* */, int tableIndex, int lineCounter);

bool handlesData(char* line, symbolTable table, int tableIndex, int lineCounter);

bool handleEntry(char* line);

bool handleExtern(char* line);

bool handleString(char* line);

bool handleCommand(char* line, symbolTable table, int tableIndex, int lineCounter, word* cs, int* ic);

bool handleArg(char* arg); // Need to complete this

char* getline(FILE*);

bool valueIsNumber(char* value, int lineCounter);

